package net.morher.javadoodle.programflow;

public class FlowTestMain {

    private final SomethingFactory factory;

    public FlowTestMain(SomethingFactory factory) {
        this.factory = factory;
    }

    public static void main(String[] args) {
        FlowTestMain testKlasse = new FlowTestMain(new EnglishSomethingFactory());

        System.out.println(testKlasse.getSomething(true));
        System.out.println(testKlasse.getSomething(false));

        FlowTestMain norwegianTestKlasse = new FlowTestMain(new NorwegianSomethingFactory());

        System.out.println(norwegianTestKlasse.getSomething(true));
        System.out.println(norwegianTestKlasse.getSomething(false));

        FlowTestMain failingTestKlasse = new FlowTestMain(null);

        System.out.println(failingTestKlasse.getSomething(true));
        System.out.println("Nesten ferdig...");
        System.out.println(failingTestKlasse.getSomething(false));

        System.out.println("FERDIG!");

    }

    private Something getSomething(boolean shouldReturnTheFirst) {
        if (shouldReturnTheFirst) {
            return factory.getFirst();
        }

        return factory.getSecond();
    }

}
