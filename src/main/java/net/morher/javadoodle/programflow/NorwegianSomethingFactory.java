package net.morher.javadoodle.programflow;

public class NorwegianSomethingFactory implements SomethingFactory {

    @Override
    public Something getFirst() {
        return Something.FORSTE;
    }

    @Override
    public Something getSecond() {
        return Something.ANDRE;
    }
}
