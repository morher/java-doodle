package net.morher.javadoodle.programflow;

public class EnglishSomethingFactory implements SomethingFactory {

    @Override
    public Something getFirst() {
        return Something.FIRST;
    }

    @Override
    public Something getSecond() {
        return Something.SECOND;
    }
}
