package net.morher.javadoodle.programflow;

public interface SomethingFactory {

    Something getFirst();

    Something getSecond();

}